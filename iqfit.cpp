//
// Created by Nolan De Souza on 10/04/2020.
//

#include "iqfit.h"
#include <exception>
#include <iostream>
#include <map>
#include <sstream>


static const std::set<colour> piece_colours{
        colour::yellow,
        colour::orange,
        colour::red,
        colour::pink,
        colour::purple,
        colour::light_blue,
        colour::blue,
        colour::dark_blue,
        colour::light_green,
        colour::green,
};

static const std::map<colour, std::string> colour_to_codes{
        {colour::yellow,      "Y "},
        {colour::orange,      "O "},
        {colour::red,         "R "},
        {colour::pink,        "Pi"},
        {colour::purple,      "Pu"},
        {colour::light_blue,  "lB"},
        {colour::blue,        "B "},
        {colour::dark_blue,   "dB"},
        {colour::light_green, "lG"},
        {colour::green,       "G "},
        {colour::none,        "* "},
        {colour::blocked,     "# "},
};


std::string get_code(colour c) {
    return colour_to_codes.at(c);
}


colour get_colour(const std::string &code) {
    for (auto[k, v]: colour_to_codes) {
        if (code == v) {
            return k;
        }
    }
    throw std::invalid_argument("Cannot get_colour. Invalid code. code=" + code);
}


piece::piece(colour c) : c{c} {
    switch (c) {
        case colour::yellow:
            setup_faces("##  ",
                        "####",
                        "   #");
            break;
        case colour::orange:
            setup_faces("  # ",
                        "####",
                        "# # ");
            break;
        case colour::red:
            setup_faces("   #",
                        "####",
                        "#  #");
            break;
        case colour::pink:
            setup_faces("  ##",
                        "####",
                        "  # ");
            break;
        case colour::purple:
            setup_faces("##  ",
                        "### ",
                        "  # ");
            break;
        case colour::light_blue:
            setup_faces("  # ",
                        "####",
                        " ## ");
            break;
        case colour::blue:
            setup_faces("   #",
                        "####",
                        " # #");
            break;
        case colour::dark_blue:
            setup_faces(" #  ",
                        "### ",
                        "# # ");
            break;
        case colour::light_green:
            setup_faces("  # ",
                        "### ",
                        "# # ");
            break;
        case colour::green:
            setup_faces(" ## ",
                        "### ",
                        " #  ");
            break;
        default:
            throw std::invalid_argument("Invalid colour: Cannot create piece.");
    }
}


void piece::show() const {
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            std::cout << (faces[current_face][current_rotation][i][j] ? get_code(c) : get_code(colour::none)) << " ";
        }
        std::cout << std::endl;
    }
}


std::array<std::array<bool, piece::cols>, piece::rows> piece::get_face_array() {
    std::array<std::array<bool, piece::cols>, piece::rows> f = {0};
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            f[i][j] = faces[current_face][current_rotation][i][j];
        }
    }
    return f;
}


void piece::rotate_face() {
    current_rotation = ++current_rotation % num_rotations;
}


void piece::change_face() {
    current_face = ++current_face % num_faces;
}


void piece::setup_faces(const char *first, const char *common, const char *last) {
    for (unsigned i = 0; i < cols; ++i) {
        auto f = faces[0][0];
        f[0][i] = first[i] == '#';
        f[1][i] = common[i] == '#';
        f[2][i] = false;
        f[3][i] = false;

        f = faces[1][0];
        f[0][i] = common[i] == '#';
        f[1][i] = last[i] == '#';
        f[2][i] = false;
        f[3][i] = false;
    }

    for (unsigned face = 0; face < num_faces; ++face) {
        for (unsigned i = 0; i < cols; ++i) {
            auto ii = cols - 1 - i;

            auto f = faces[face][0];

            auto fr = faces[face][1];
            fr[ii][0] = f[0][i];
            fr[ii][1] = f[1][i];
            fr[ii][2] = f[2][i];
            fr[ii][3] = f[3][i];

            fr = faces[face][2];
            fr[3][ii] = f[0][i];
            fr[2][ii] = f[1][i];
            fr[1][ii] = f[2][i];
            fr[0][ii] = f[3][i];

            fr = faces[face][3];
            fr[i][3] = f[0][i];
            fr[i][2] = f[1][i];
            fr[i][1] = f[2][i];
            fr[i][0] = f[3][i];
        }
    }
}

board::board() {
    std::cout << "Created empty board." << std::endl;
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            if (i < 2 || i > 6 || j < 2 || j > 11) {
                grid[i][j] = colour::blocked;
            } else {
                grid[i][j] = colour::none;
            }
        }
    }
    show();
}

board::board(const std::array<std::string, 5> &layout) {
    std::cout << "Created populated board." << std::endl;

    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            if (i < 2 || i > 6 || j < 2 || j > 11) {
                grid[i][j] = colour::blocked;
            }
        }
    }

    for (unsigned i = 0; i < rows - 4; ++i) {
        std::string token;
        std::vector<std::string> tokens;
        std::istringstream iss(layout[i]);
        while (std::getline(iss, token, ' ')) {
            if (token.size() > 0) {
                tokens.push_back(token);
            }
        }

        for (unsigned j = 0; j < cols - 4; ++j) {
            std::string code = tokens[j];
            if (code.size() == 1) {
                code += " ";
            }
            auto c = get_colour(code);
            grid[i + 2][j + 2] = c;
        }
    }
    show();
}


void board::show() const {
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            std::cout << get_code(grid[i][j]) << " ";
        }
        std::cout << std::endl;
    }
}


std::set<colour> board::get_used_colours() const {
    std::set<colour> used_colours;
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            auto c = grid[i][j];
            if (c != colour::blocked && c != colour::none) {
                used_colours.insert(c);
            }
        }
    }
    return used_colours;
}


bool board::find_piece_position(ppiece &p, unsigned &start_row, unsigned &start_col) {
    auto r = start_row;
    auto c = start_col;
    while (p->get_current_face() < p->num_faces) {
        while (p->get_current_rotation() < p->num_rotations) {
            while (r < rows - 4) {
                while (c < cols - 4) {
                    if (fit_piece_array_at_position(p, r, c)) {
                        start_row = r;
                        start_col = c;
                        start_col++;
                        return true;
                    }
                    ++c;
                }
                ++r;
                c = 0;
            }
            p->rotate_face();
            r = 0;
            c = 0;
            if (p->get_current_rotation() == 0) {
                break;
            }
        }
        p->change_face();
        if (p->get_current_face() == 0) {
            break;
        }
    }
    return false;
}

bool board::fit_piece_array_at_position(ppiece &p, unsigned row, unsigned col) {
    auto f = p->get_face_array();
    bool fitted = true;
    for (unsigned i = 0; i < p->rows; ++i) {
        for (unsigned j = 0; j < p->cols; ++j) {
            if (f[i][j] && grid[row + i][col + j] != colour::none) {
                fitted = false;
                break;
            }
        }
        if (!fitted) {
            break;
        }
    }
    if (fitted) {
        std::cout << "Fitting piece at [" << row << ", " << col << "]" << std::endl;
        p->show();
        for (unsigned i = 0; i < p->rows; ++i) {
            for (unsigned j = 0; j < p->cols; ++j) {
                if (f[i][j]) {
                    grid[row + i][col + j] = p->get_colour();
                }
            }
        }
    }

    return fitted;
}


bool board::is_filled() const {
    for (unsigned i = 0; i < rows; ++i) {
        for (unsigned j = 0; j < cols; ++j) {
            if (grid[i][j] == colour::none) {
                return false;
            }
        }
    }
    return true;
}


game::game() {
    setup_remaining_pieces();
}


game::game(const std::array<std::string, 5> &layout) : b{layout} {
    setup_remaining_pieces();
}

bool game::solve() {
    return solve(0, b, remaining);
}

bool game::solve(int recursion_level, const board &b, const std::vector<ppiece> &remaining) {
    if (remaining.size() == 0) {
        std::cout << "No remaining pieces." << std::endl;
        return false;
    }

    auto b_to_change = b;
    auto remaining_to_change = remaining;

    ppiece p = remaining_to_change.back();
    remaining_to_change.pop_back();

    unsigned start_row = 0;
    unsigned start_col = 0;
    while (b_to_change.find_piece_position(p, start_row, start_col)) {
        std::cout << "Recursion level: " << recursion_level << std::endl;
        b_to_change.show();

        if (b_to_change.is_filled()) {
            std::cout << "Solved!" << std::endl;
            return true;
        }

        if (solve(recursion_level + 1, b_to_change, remaining_to_change)) {
            return true;
        }

        b_to_change = b;
    }
    return false;
}


void game::setup_remaining_pieces() {
    auto all_colours = piece_colours;

    for (auto c: b.get_used_colours()) {
        all_colours.erase(c);
    }

    for (auto c: all_colours) {
        remaining.push_back(std::make_shared<piece>(c));
    }

    std::cout << "Remaining pieces: ";
    for (auto &p: remaining) {
        std::cout << get_code(p->get_colour()) << " ";
    }
    std::cout << std::endl;
}

