//
// Created by Nolan De Souza on 10/04/2020.
//

#include "iqfit.h"
#include <fstream>
#include <iostream>


int main() {
    std::cout << "Hello, World! I'm the IQ FIT solver!" << std::endl;
    std::cout << "Check out IQ FIT at https://www.smartgames.eu/uk/one-player-games/iq-fit" << std::endl;

    std::ifstream ifs("../input.txt");

    if (!ifs) {
        std::cout << "Could not read ../input.txt." << std::endl;
        return 1;
    }

    std::array<std::string, 5> layout;
    std::cout << "Reading ../input.txt." << std::endl;
    char line[50];
    unsigned i = 0;
    while (ifs.getline(line, 50)) {
        std::cout << line << std::endl;
        layout[i] = line;
        ++i;
    }

    game g(layout);

    if(!g.solve()) {
        std::cout << "Failed to solve." << std::endl;
        return 1;
    }
    return 0;
}
