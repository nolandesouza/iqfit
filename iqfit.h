//
// Created by Nolan De Souza on 10/04/2020.
//

#pragma once

#include <array>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

enum class colour {
    yellow,
    orange,
    red,
    pink,
    purple,
    light_blue,
    blue,
    dark_blue,
    light_green,
    green,
    none,
    blocked
};

std::string get_code(colour c);

colour get_colour(const std::string &code);


class piece {
public:
    constexpr const static unsigned num_faces = 2;
    constexpr const static unsigned num_rotations = 4;
    constexpr const static unsigned rows = 4;
    constexpr const static unsigned cols = 4;

    piece(colour c);

    colour get_colour() const {
        return c;
    }

    unsigned get_current_face() const {
        return current_face;
    }

    unsigned get_current_rotation() const {
        return current_rotation;
    }

    void show() const;

    std::array<std::array<bool, cols>, rows> get_face_array();

    void rotate_face();

    void change_face();

private:
    void setup_faces(const char *first, const char *common, const char *last);

    const colour c;
    bool faces[num_faces][num_rotations][rows][cols] = {0};
    unsigned current_face{0};
    unsigned current_rotation{0};
};


using ppiece = std::shared_ptr<piece>;


class board {
public:
    board();

    board(const std::array<std::string, 5> &layout);

    void show() const;

    std::set<colour> get_used_colours() const;

    bool find_piece_position(ppiece &p, unsigned &start_row, unsigned &start_col);

    bool is_filled() const;

private:
    bool fit_piece_array_at_position(ppiece &p, unsigned row, unsigned col);

    constexpr const static unsigned rows = 9;
    constexpr const static unsigned cols = 14;
    colour grid[rows][cols];
};


class game {
public:
    board b;

    game();

    game(const std::array<std::string, 5> &layout);

    bool solve();

private:
    static bool solve(int recursion_level, const board &b, const std::vector<ppiece> &remaining);

    void setup_remaining_pieces();

    std::vector<ppiece> remaining;
};

